SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE IF NOT EXISTS `jbug_issues` (
  `issue_id` int(255) NOT NULL AUTO_INCREMENT,
  `project_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `date_created` varchar(20) NOT NULL,
  `date_closed` varchar(20) NOT NULL,
  `open` int(11) NOT NULL DEFAULT '1',
  `priority` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`issue_id`),
  KEY `project` (`project_id`),
  KEY `user` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

INSERT INTO `jbug_issues` (`issue_id`, `project_id`, `user_id`, `title`, `description`, `date_created`, `date_closed`, `open`, `priority`) VALUES
(1, 1, 1, 'Broke everything. Ever. After.', 'Everything just happened to break. Not sure how it happened but I''m rather ticked off about it.', '2014-04-20', '0000-00-00', 1, 0),
(2, 1, 1, 'Broke things again', 'Everything is spiders', '2014-04-16', '0000-00-00', 1, 0),
(11, 1, 1, 'This is a test of a really long project title. I made it this way because I figured it''d break the l', 'ok', '2014-04-24', '', 1, 0),
(12, 2, 1, 'Another issue for a different project', 'Great!', '2014-05-12', '', 1, 0);

CREATE TABLE IF NOT EXISTS `jbug_projects` (
  `project_id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `date_created` datetime NOT NULL,
  `user_id` int(255) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

INSERT INTO `jbug_projects` (`project_id`, `title`, `date_created`, `user_id`) VALUES
(1, 'My Great Project', '2014-04-20 14:11:22', 1),
(2, 'Cheese Sauce Fillet', '2014-04-22 11:00:00', 1);

CREATE TABLE IF NOT EXISTS `jbug_users` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

INSERT INTO `jbug_users` (`user_id`, `name`, `email`, `password`, `date_created`) VALUES
(1, 'admin', 'admin@andrewnatoli.com', 'admin', '2014-04-19 23:51:00');


ALTER TABLE `jbug_issues`
  ADD CONSTRAINT `project` FOREIGN KEY (`project_id`) REFERENCES `jbug_projects` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user` FOREIGN KEY (`user_id`) REFERENCES `jbug_users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;