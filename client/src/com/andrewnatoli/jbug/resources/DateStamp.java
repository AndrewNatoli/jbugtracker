package com.andrewnatoli.jbug.resources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateStamp {

    /**
     * getTodaysDate - Returns the current date in yyyy-mm-dd format
     * @return String yyyy-mm-dd date
     */
    public static String getTodaysDate() {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date today = Calendar.getInstance().getTime();
        return df.format(today);
    }
}
